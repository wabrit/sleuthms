package example.sleuthms.models.foo;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

interface FooEventSource {

  @Output("fooEventsChannel")
  MessageChannel events();

}
