package example.sleuthms.models.foo;

public interface FooService {

  void invoke();

  void triggerMessage();

}
