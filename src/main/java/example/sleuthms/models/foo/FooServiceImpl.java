package example.sleuthms.models.foo;

import brave.Span;
import brave.Tracer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service("fooService")
@EnableBinding(FooEventSource.class)
class FooServiceImpl implements FooService {

  @Autowired
  private FooEventSource eventSource;

  @Autowired
  private Tracer tracer;

  @Autowired
  private FooService self;

  @Override
  @NewSpan
  public void invoke() {
    self.triggerMessage();
  }

  @Override
  @NewSpan
  public void triggerMessage() {
    // Fire off an event containing the current span id
    final Span currentSpan = this.tracer.currentSpan();
    assert currentSpan.context().traceIdString() != null;
    assert currentSpan.context().spanIdString() != null;
    assert currentSpan.context().parentIdString() != null;
    eventSource.events().send(MessageBuilder.withPayload(currentSpan.context().spanIdString()).build());
  }

}
