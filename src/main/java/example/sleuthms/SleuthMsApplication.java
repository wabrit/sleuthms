package example.sleuthms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SleuthMsApplication {

  public static void main(final String[] args) {
    SpringApplication.run(SleuthMsApplication.class, args);
  }

}
