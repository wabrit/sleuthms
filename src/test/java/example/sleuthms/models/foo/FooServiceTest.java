package example.sleuthms.models.foo;

import brave.baggage.BaggagePropagation;
import brave.propagation.B3Propagation;
import brave.propagation.B3SingleFormat;
import brave.propagation.TraceContext;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class FooServiceTest {

  /**
   * Override the default format for producer to include a parent span. Without this
   * we fail to get parent span included in the b3 header of outgoing RabbitMQ
   * messages.
   * @see <a href="https://stackoverflow.com/questions/65326619/sleuth-does-not-include-parentspan-in-b3-single-header-for-message-to-rabbitmq">this stackoverflow question</a>
   */
  @TestConfiguration // Comment out this annotation to see the error
  protected static class B3Configuration {
    @Bean
    BaggagePropagation.FactoryBuilder baggagePropagationFactoryBuilder() {
      return BaggagePropagation.newFactoryBuilder(
          B3Propagation.newFactoryBuilder()
              .injectFormat(B3Propagation.Format.SINGLE)
              .build()
      );
    }
  }

  @Autowired
  private FooService fooService;

  @Autowired
  private FooEventSource eventSource;

  @Autowired
  private MessageCollector messageCollector;

  @AfterEach
  public void cleanUp() {
    messageCollector.forChannel(eventSource.events()).clear();
  }

  @Test
  void testTriggerMessage() {

    // Invoke the service, which will in turn cause a message to be sent
    fooService.invoke();

    @SuppressWarnings("unchecked")
    final Message<String> message = (Message<String>) messageCollector.forChannel(eventSource.events()).poll();
    assertThat("Expecting an outgoing message", message, notNullValue());

    final MessageHeaders headers = message.getHeaders();
    final String b3Header = headers.get("b3", String.class);
    assertThat("Expecting a b3 header in the outgoing message", b3Header, notNullValue());

    // Examine the b3 header in the outgoing message
    final TraceContext context = B3SingleFormat.parseB3SingleFormat(b3Header).context();
    final String parentSpanId = context.parentIdString();

    assertThat("Expecting a trace id in the b3 message header", context.traceIdString(), notNullValue());
    assertThat("Expecting a span id in the b3 message header", context.spanIdString(), notNullValue());

    // The next assertion will fail if we have not overridden the default B3 propagation in the @TestConfiguration class above
    assertThat("Expecting a parent span id in the b3 message header", parentSpanId, notNullValue());

    // We expect that the payload contained the span in effect within the context of the service
    // call, and that that spanId should now be the parentSpanId in the b3 header
    assertThat("Expecting parent span id in b3 message header to be equal to the span id in service context",
        message.getPayload(), equalTo(parentSpanId));
  }

}
