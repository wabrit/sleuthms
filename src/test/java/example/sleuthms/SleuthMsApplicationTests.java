package example.sleuthms;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SleuthMsApplicationTests {

	@Test
	public void contextLoads() {
	  // Checks that a Spring Context load doesn't raise any errors
	}

}
