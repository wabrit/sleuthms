# Sleuth Example

This project demonstrates the issue outlined in
  https://stackoverflow.com/questions/65326619/sleuth-does-not-include-parentspan-in-b3-single-header-for-message-to-rabbitmq

A simple service is used to generate an outgoing AMQP message that should contain a b3 header. At the point the message
is generated there is a span and parent span in effect (this is done having the test call through 2 services methods, each annotated
with @NewSpan).

It is expected that this becomes the parent span in the b3 header (a new span being generated to send the message).

i.e.
 - Within FooServiceImpl.triggerMessage: traceId=T, spanId=S2, parentSpanId=S1
 - Message header on RabbitMQ queue: b3=T-S3-1-S2 (same trace, new span, parent set to span in effect within service).

The FooServiceTest checks that:

 - the outgoing message has a b3 header, and
 - that it contains a trace, span and parent id, and
 - the parent id is equal to the span id in effect within the service

However it only works by dint of an explicit configuration bean that ensures B3Propagation.Format.SINGLE is used for propagation.

Without this explicit configuration, the test fails because the outgoing message has no parent id. (in fact it has the same
trace id as was in effect in the service, and an entirely new span id, but has no link to the originating span).

I believe this is because by default B3Propagation.Format.SINGLE_NO_PARENT is being used for propagation.